## Projeto Crud

* Foi proposto fazer um CRUD para os produtos e clientes. 

## Estrutura do projeto

src/Components/Templates ---> Há dois folders contendo o Header e a Página principal do projeto, onde será renderizado a página principal.

src/Components/AddresForm, CustomerForm, ProductsForm ---> Diretórios responsáveis por renderizar os respectivos CRUDs.

src/Components/Home ---> Página principal do projeto, onde deverá ser a página principal antes de o usuário interagir com algum botão.

## Frameworks e utilitarios utilizados

Nesse projeto, foram usados: React.js, Swal, Bootstrap.

## Funcionalidade Esperada

É esperado que seja quem for testar, consiga salvar os dados de cada CRUD localmente no seu browser. Dentro de cada index dos respectivos formulários, há validações
para os campos e se for válido, deverá salvar no localStorage.

## Observações

Se tratando de um form com componentes controlados, no método de cancelar formulário, precisei explicitamente pegar o objeto por objeto e setar vazio nele.