import React from 'react'
import './index.css'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'


import { useState } from 'react'


const AddressForm = () => {

    /* --------  Methods -------- */

    const MySwal = withReactContent(Swal)

    const [addressValues, setAddress] = useState({
        address: '',
        city: '',
        state: ''
    })

    const handlerValues = (e) => {
        const value = e.target.value;
        setAddress({
            ...addressValues,
            [e.target.name]: value
        })
    }

    const saveItemToStorage = () => {
        validate()
        localStorage.setItem(
            `Address`,
            JSON.stringify(addressValues)
        )
        // using that function to reset fields after submit 
        cancelForm()
    }

    const cancelForm = () => {
        setAddress({
            address: '',
            city: '',
            state: '',
        })
    }

    const validate = () => {
        if (addressValues.address === '' ||
             addressValues.city === '' || 
             addressValues.state === '' ) {
            return MySwal.fire({
                title: 'Oh não :(',
                text: 'Não foi possível salvar seus dados, verifique e tente novamente.',
                icon: 'error',
                confirmButtonText: 'Preencher de novo',
                confirmButtonColor: 'red'
            })
        } else {
            MySwal.fire({
                title: 'Parabéns',
                text: 'Os dados foram salvos com sucesso',
                icon: 'success',
                confirmButtonText: 'Entendi',
                confirmButtonColor: 'blue'
            })
        }
    }

    /* -------- End Methods -------- */

    return (
        <>
            <div className="form form-address">
                    <div className="row">
                        <div className="col-12 col-md-12">
                            <div className="form-group">
                                <label> Endereço </label>
                                <input
                                    type="text" className="form-control"
                                    name="address"placeholder="Digite o endereço" id="address" value={addressValues.address} onChange={handlerValues}
                                ></input>
                            </div>
                        </div>
                        <div className="col-12 col-md-6">
                            <div className="form-group">
                                <label> Cidade </label>
                                <input
                                    type="text" className="form-control"
                                    name="city" placeholder="Digite a cidade" id="city" onChange={handlerValues} value={addressValues.city}
                                ></input>
                            </div>
                        </div>
                        <div className="col-12 col-md-6">
                            <div className="form-group">
                                <label> Estado </label>
                                <input
                                    type="text" className="form-control"
                                    name="state" placeholder="Digite o estado" id="state" onChange={handlerValues} value={addressValues.state}
                                ></input>
                            </div>
                        </div>
                    <hr className="address-row" />
                    <div className="row">
                        <div className="col-12 d-flex justify-content-end">
                            <button className="btn btn-primary" onClick={saveItemToStorage}>
                                Salvar
                            </button>
                            <button className="btn btn-secundary ml-2" onClick={cancelForm}>
                                Cancelar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AddressForm
