import React from 'react'
import './index.css'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

import { useState } from 'react'

const ProductForm = () => {

    /* -------- Methods -------- */

    const MySwal = withReactContent(Swal)

    const [ product, setProduct ] = useState('')
    const [ quant, setQuant ] = useState('1')

    const saveItemToStorage = () => {
        validate()
        const savedProductsData = [product, quant];
        localStorage.setItem('Products and quantity', JSON.stringify(savedProductsData))
        // using that function to reset fields after submit 
        clearFields()
    }

    const clearFields = () => {
        setProduct('');
        setQuant('')
    }

    const validate = () => {
        if (product === '' ||
             quant === '' ) {
            return MySwal.fire({
                title: 'Oh não :(',
                text: 'Não foi possível salvar seus dados, verifique e tente novamente.',
                icon: 'error',
                confirmButtonText: 'Preencher de novo',
                confirmButtonColor: 'red'
            })
        } else {
            MySwal.fire({
                title: 'Parabéns',
                text: 'Os dados foram salvos com sucesso',
                icon: 'success',
                confirmButtonText: 'Entendi',
                confirmButtonColor: 'blue'
            })
        }
    }

    /* -------- End Methods --------*/

    return (
        <>
            <div className="form products-form">
                    <div className="row">
                        <div className="col-12 col-md-8">
                            <div className="form-group">
                                <label> Nome do Produto </label>
                                <input
                                    type="text" className="form-control"
                                    name="name-product"placeholder="Digite o nome do produto"
                                    onChange={e => setProduct(e.target.value)} value={product}
                                ></input>
                            </div>
                        </div>
                        <div className="col-12 col-md-4">
                            <div className="form-group">
                                <label> Quantidade </label>
                                <select name="" id="quantity" className="form-control"
                                    defaultValue="1" onChange={e => setQuant(e.target.value)}
                                    value={quant}
                                >
                                    <option value="1"> 1 </option>
                                    <option value="2"> 2 </option>
                                    <option value="3"> 3 </option>
                                    <option value="4"> 4 </option>
                                    <option value="5"> 5 </option>
                                </select>
                            </div>
                        </div>
                    <hr className="product-row" />
                    <div className="row">
                        <div className="col-12 d-flex justify-content-end">
                            <button className="btn btn-primary" onClick={saveItemToStorage}>
                                Salvar
                            </button>
                            <button className="btn btn-secundary ml-2" onClick={clearFields}>
                                Cancelar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProductForm
