import React from 'react'
import Main from '../Templates/Main/index'
import './index.css'

import { Link } from 'react-router-dom'


const Home = () => {
    return (
        <Main icon="home" title="Inicio" subtitle="Cadastros">
            <hr />
            <p className="mb-0 subtitle text-center"> Sistema para cadastrar produtos e clientes.</p>
            <hr />
            <div className="d-flex justify-content-around">  
                <Link to="/users">
                    <button type="button" className="btn btn-primary"> Cadastrar Cliente </button>
                </Link>
                <Link to="/products">
                    <button type="button" className="btn btn-primary"> Cadastrar Produto </button>
                </Link>
                <Link to="/address">
                    <button type="button" className="btn btn-primary"> Cadastrar Endereço </button>
                </Link>
            </div>
        </Main>
    )
}

export default Home
