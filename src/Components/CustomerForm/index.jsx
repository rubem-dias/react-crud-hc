import React from 'react'
import './index.css'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

import { useState } from 'react'

const CustomerForm = () => {

    const MySwal = withReactContent(Swal)

    const [ customerData, setCustomerData ] = useState({
        name: '',
        email: '',
        phone: '',
        username: '',
        cpf: ''
    })

    const handlerValues = (e) => {
        const value = e.target.value;
        setCustomerData({
            ...customerData,
            [e.target.name]: value
        })
    }

    const saveItemToStorage = () => {
        validate()
        localStorage.setItem(
            `Costumers`,
            JSON.stringify(customerData)
        )
        // using that function to reset fields after submit 
        cancelForm()
    }

    const cancelForm = () => {
        setCustomerData({
            name: '',
            email: '',
            cpf: '',
            phone: ''
        })
    }

    const validate = () => {
        if (customerData.name === '' ||
             customerData.email === '' || 
             customerData.cpf === '' ||
             customerData.phone === '') {
            return MySwal.fire({    
                title: 'Oh não :(',
                text: 'Não foi possível salvar seus dados, verifique e tente novamente.',
                icon: 'error',
                confirmButtonText: 'Preencher de novo',
                confirmButtonColor: 'red'
            })
        } else {
            MySwal.fire({
                title: 'Parabéns',
                text: 'Os dados foram salvos com sucesso',
                icon: 'success',
                confirmButtonText: 'Entendi',
                confirmButtonColor: 'blue'
            })
        }
    }

    return (
        <>
        <div className="form form-customer" id="form">
                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label> Nome </label>
                            <input
                                type="text" className="form-control"
                                name="name"placeholder="Digite o nome"
                                value={customerData.name} onChange={handlerValues}
                            ></input>
                        </div>
                    </div>
                    <div className="col-12 col-md-6">
                         <div className="form-group">
                             <label> Email </label>
                             <input
                                type="text" className="form-control"
                                name="email" placeholder="Digite o email"
                                value={customerData.email} onChange={handlerValues}
                             ></input>
                         </div>
                    </div>
                    <div className="col-12 col-md-6">
                         <div className="form-group">
                             <label> Celular </label>
                             <input
                                type="number" className="form-control"
                                name="phone" placeholder="Digite o celular"
                                value={customerData.phone} onChange={handlerValues}
                             ></input>
                         </div>
                    </div>
                    <div className="col-12 col-md-6">
                         <div className="form-group">
                             <label> CPF </label>
                             <input
                                type="text" className="form-control"
                                name="cpf" placeholder="Digite o CPF"
                                value={customerData.cpf} onChange={handlerValues}
                            ></input>
                         </div>
                    </div>
                </div>

                <hr />

                <div className="row">
                    <div className="col-12 d-flex justify-content-end">
                        <button className="btn btn-primary" onClick={saveItemToStorage}>
                             Salvar
                         </button>
                        <button className="btn btn-secundary ml-2" onClick={cancelForm}>
                             Cancelar
                        </button>
                    </div>
                </div>

            </div>
        </>
    )
}

export default CustomerForm
