
import React from 'react'

import { Switch, Route } from 'react-router-dom'
import CustomerForm from './Components/CustomerForm/index'
import AddressForm from './Components/AddressForm/index'
import ProductForm from './Components/ProductsForm/index'


const Routes = () => {
    return (
        <Switch>
            <Route exact path="/users" component={CustomerForm}></Route>
            <Route exact path="/address" component={AddressForm}></Route>
            <Route exact path="/products" component={ProductForm}></Route>
        </Switch>
    )
}

export default Routes