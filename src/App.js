import React from "react";
import Home from './Components/Home/index'
import Routes from "./Routes.jsx";

import { BrowserRouter } from 'react-router-dom'

function App() {

  return (
    <BrowserRouter>

      <div className="App">
          <Home />
          <Routes />
      </div>
      
    </BrowserRouter>
  );
}

export default App;
